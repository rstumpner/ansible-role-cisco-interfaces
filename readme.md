# Ansible Role Cisco Interface Config

This is a Ansible Role for Interface Settings on Cisco Devices.

Aufbau:
- Build Cisco Interface Config
  - All Availiable Build Modules
    - ansible_modes: all
    - tags: 
      - build
  - Build Cisco Interface Config
    - ansible_modes: all
    - tags:
      - build
      - cli
- Testing Device Config
  - Snapshot of Device Running Config and write to Startup
    - ansible_modes: all
    - tags:
      - bak
      - state
  - Snapshot of Device state Config Backup
    - ansible_modes: all
    - tags:
      - bak
      - state
  - Review Config Changes (--diff)
    - ansible_modes: --diff
    - tags:
      - diff
      - bak
      - state
  - Testing Config Changes
    - ansible_modes: all
    - tags:
      - tests
  - Deploy Device Config
    - not ansible check-mode
    - tags:
      - deploy
  - Deploy Config with CLI only Operations and Management ( Port Descriptions / no Dataplane / no Controllplane )
    - not ansible check-mode
    - tags:
      - deploy,cli,oam 
  - Deploy Config with CLI only Operations and Management ( Port Descriptions / Dataplane / no Controllplane )
    - not ansible check-mode
    - tags:
      - deploy,cli,dp 

Requirements:
    None

Role Variable Examples:

# GigabitEthernet Switchport L2 Access with Vlan
  GigabitEthernet0/0:
      description: "Cust: vlanid / name (Raumnummer/Hostname) {Patchpanelbeschriftung} [1Gbit]"
      switchport_mode: access
      vlan_id: 10

# GigabitEthernet Switchport L2 Access with Vlan (DEPRECATED)
  GigabitEthernet1/45:
    desc: "Cust: wel-ca-inf-appletv (C_HS-106/WEL-C-1OG-006) {C/O1-006/02}"
    config: access
    vlanid: 196

Using this Role:
Drive to Ansible Role Directory:
    - git clone https://fhzengitlab1.fhooe.at/P50010/cisco-interfaces.git

Aktivate Role in a Playbook:

Example:
```YAML
- hosts: all
  roles:
     - cisco-interfaces
```

Tested:
 - Cisco IOS
 - Cisco IOS-XE
 
License:
    MIT / BSD

Author Information:
roland@stumpner.at